How to contribute to zoomrlib
=============================

This document explains how to contribute to zoomrlib.


Bugfix and features
-------------------

If you find a bug or if you want a new feature, please create an issue
in the [issue tracker for this
project](https://gitlab.com/remytmns/zoomrlib/-/issues).

If it's a bug, provide step to reproduce. If possible provide the ZDT
that shows the bug. The bug will be reproduced, and confirmed.

If it's a feature request, explain the needs behind the feature. If you
plan to develop it, explain a bit how you will add this feature to the
project. The feature will be discussed, and the way to integrate it into
the project also. After that, the feature request will be confirmed.

When a bug or feature request is confirmed, feel free to open a merge
request and reference the issue on it.


Development tools installation
------------------------------

### poetry

This project uses [poetry](https://python-poetry.org) as dependency and
project management.

Install with:

```sh
pipx install poetry
```

Then to install the project with its development dependencies, run this
command in the root directory of the project:

```sh
poetry install
```

It will uses existing virtual env if it is activated (see below).


### virtualenv

Make sure to install the project in a virtual environment. Either with
the `venv` module of python or with `poetry`.


### pre-commit

`pre-commit` is a tool that run checks on each commit done with git.

Install with:

```sh
pipx install pre-commit
```

Then, in the root directory of the project (the root of git repo):

```sh
pre-commit install
```

Now pre-commit will run checks each time a commit is made, and it will
forbid to create a commit if code does not pass checks.

Checks are done with:

- black
- isort
- pylint
- other checks on files

See `.pre-commit.yaml` file in the root directory of the project.


### pylint

`pylint` is used as a linter and checks for bad practices in the code.
It must be installed into the virtual environment in order to work properly.
Normally it's installed as a development dependency using `poetry`.


### towncrier

`towncrier` is used to track changes made at each versions.

Maintainers use it to generate the `CHANGES.md` file.

Please provide news newsfragments with your merge request.

You can create a newsfragment with (run in the root directory of the
project):

```sh
towncrier create <slug-of-your-feature.{feature,bugfix,doc,removal,misc}
```

See `towncrier create --help` for more info and/or consult [towncrier
documentation](.https://towncrier.readthedocs.io/en/stable/index.html).

**Note:** Do not build the CHANGES.md file with `towncrier build`, this
operation is done only by maintainers when publishing a stable release.
