# Copyright 2019-2020 Rémy Taymans <remytms@tsmail.eu>
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

"""Test for zoomrlib library, writing on a project file"""

from pathlib import Path

import pytest

import zoomrlib

__testdir__ = Path(__file__).parent


def test_write_project():
    """Test for writing a project file."""
    prjname = __testdir__ / "data" / "PRJDATA.ZDT"
    with zoomrlib.open(prjname, "r") as file:
        project = zoomrlib.project.load(file)
    # header
    with pytest.raises(TypeError):
        project.header = 123
    with pytest.raises(ValueError):
        project.header = "this is a very long project name" * 10
    with pytest.raises(ValueError):
        project.header = "zőőm r-16"
    project.header = "ZOOM R-16  PROJECT DATA VER0002"

    # name
    with pytest.raises(TypeError):
        project.name = 123
    with pytest.raises(ValueError):
        project.name = "Prőjèçt"
    with pytest.raises(ValueError):
        project.name = "tolongprojectname"
    project.name = "NEWPROJ0"

    # bitlength
    with pytest.raises(TypeError):
        project.bitlength = "16bits"
    with pytest.raises(ValueError):
        project.bitlength = 32
    project.bitlength = 24

    # protected
    project.protected = True

    # insert effect
    project.insert_effect_on = False

    # file
    with pytest.raises(TypeError):
        project.tracks[0].file = 123
    with pytest.raises(ValueError):
        project.tracks[0].file = "THIS_IS_A_VERY_TOO_LONG_NAME.WAV"
    with pytest.raises(ValueError):
        project.tracks[0].file = "ÑOÑ_ÀSÇÏÏ"
    with pytest.raises(ValueError):
        project.tracks[0].file = "not accepted"
    project.tracks[0].file = "BASS-001.WAV"

    # status
    with pytest.raises(ValueError):
        project.tracks[0].status = "a wrong status"
    project.tracks[0].status = zoomrlib.REC
    project.tracks[1].status = zoomrlib.PLAY
    project.tracks[2].status = zoomrlib.MUTE
    project.tracks[3].status = zoomrlib.REC
    project.tracks[4].status = zoomrlib.PLAY
    project.tracks[5].status = zoomrlib.MUTE
    project.tracks[6].status = zoomrlib.REC
    project.tracks[7].status = zoomrlib.PLAY
    project.tracks[8].status = zoomrlib.MUTE
    project.tracks[9].status = zoomrlib.REC
    project.tracks[10].status = zoomrlib.PLAY
    project.tracks[11].status = zoomrlib.MUTE
    project.tracks[12].status = zoomrlib.REC
    project.tracks[13].status = zoomrlib.PLAY
    project.tracks[14].status = zoomrlib.MUTE
    project.tracks[15].status = zoomrlib.REC

    # stereo
    project.tracks[0].stereo_on = False
    project.tracks[1].stereo_on = False
    project.tracks[2].stereo_on = False
    project.tracks[3].stereo_on = False
    project.tracks[4].stereo_on = False
    project.tracks[5].stereo_on = False
    project.tracks[6].stereo_on = False
    project.tracks[7].stereo_on = False
    project.tracks[8].stereo_on = False
    project.tracks[9].stereo_on = False
    project.tracks[10].stereo_on = True
    project.tracks[11].stereo_on = True
    project.tracks[12].stereo_on = True
    project.tracks[13].stereo_on = True
    project.tracks[14].stereo_on = True
    project.tracks[15].stereo_on = True

    # invert
    project.tracks[0].invert_on = False
    project.tracks[1].invert_on = False
    project.tracks[2].invert_on = False
    project.tracks[3].invert_on = False
    project.tracks[4].invert_on = False
    project.tracks[5].invert_on = False
    project.tracks[6].invert_on = False
    project.tracks[7].invert_on = False
    project.tracks[8].invert_on = False
    project.tracks[9].invert_on = False
    project.tracks[10].invert_on = True
    project.tracks[11].invert_on = True
    project.tracks[12].invert_on = True
    project.tracks[13].invert_on = True
    project.tracks[14].invert_on = True
    project.tracks[15].invert_on = True

    # pan
    project.tracks[0].pan = -100
    project.tracks[1].pan = -50
    project.tracks[2].pan = -50
    project.tracks[3].pan = -50
    project.tracks[4].pan = -50
    project.tracks[5].pan = 0
    project.tracks[6].pan = 0
    project.tracks[7].pan = 0
    project.tracks[8].pan = 0
    project.tracks[9].pan = 0
    project.tracks[10].pan = 50
    project.tracks[11].pan = 50
    project.tracks[12].pan = 50
    project.tracks[13].pan = 50
    project.tracks[14].pan = 50
    project.tracks[15].pan = 100

    # fader
    project.tracks[0].fader = -120
    project.tracks[1].fader = 0
    project.tracks[2].fader = 0
    project.tracks[3].fader = 0
    project.tracks[4].fader = 0
    project.tracks[5].fader = 50
    project.tracks[6].fader = 50
    project.tracks[7].fader = 50
    project.tracks[8].fader = 50
    project.tracks[9].fader = 50
    project.tracks[10].fader = 127
    project.tracks[11].fader = 127
    project.tracks[12].fader = 127
    project.tracks[13].fader = 127
    project.tracks[14].fader = 127
    project.tracks[15].fader = 300

    # chorus_on
    project.tracks[0].chorus_on = False
    project.tracks[1].chorus_on = False
    project.tracks[2].chorus_on = False
    project.tracks[3].chorus_on = False
    project.tracks[4].chorus_on = False
    project.tracks[5].chorus_on = False
    project.tracks[6].chorus_on = False
    project.tracks[7].chorus_on = False
    project.tracks[8].chorus_on = False
    project.tracks[9].chorus_on = False
    project.tracks[10].chorus_on = True
    project.tracks[11].chorus_on = True
    project.tracks[12].chorus_on = True
    project.tracks[13].chorus_on = True
    project.tracks[14].chorus_on = True
    project.tracks[15].chorus_on = True

    # chorus_gain
    project.tracks[0].chorus_gain = -120
    project.tracks[1].chorus_gain = 0
    project.tracks[2].chorus_gain = 0
    project.tracks[3].chorus_gain = 0
    project.tracks[4].chorus_gain = 0
    project.tracks[5].chorus_gain = 50
    project.tracks[6].chorus_gain = 50
    project.tracks[7].chorus_gain = 50
    project.tracks[8].chorus_gain = 50
    project.tracks[9].chorus_gain = 50
    project.tracks[10].chorus_gain = 100
    project.tracks[11].chorus_gain = 100
    project.tracks[12].chorus_gain = 100
    project.tracks[13].chorus_gain = 100
    project.tracks[14].chorus_gain = 100
    project.tracks[15].chorus_gain = 300

    # reverb_on
    project.tracks[0].reverb_on = False
    project.tracks[1].reverb_on = False
    project.tracks[2].reverb_on = False
    project.tracks[3].reverb_on = False
    project.tracks[4].reverb_on = False
    project.tracks[5].reverb_on = False
    project.tracks[6].reverb_on = False
    project.tracks[7].reverb_on = False
    project.tracks[8].reverb_on = False
    project.tracks[9].reverb_on = False
    project.tracks[10].reverb_on = True
    project.tracks[11].reverb_on = True
    project.tracks[12].reverb_on = True
    project.tracks[13].reverb_on = True
    project.tracks[14].reverb_on = True
    project.tracks[15].reverb_on = True

    # reverb_gain
    project.tracks[0].reverb_gain = -120
    project.tracks[1].reverb_gain = 0
    project.tracks[2].reverb_gain = 0
    project.tracks[3].reverb_gain = 0
    project.tracks[4].reverb_gain = 0
    project.tracks[5].reverb_gain = 50
    project.tracks[6].reverb_gain = 50
    project.tracks[7].reverb_gain = 50
    project.tracks[8].reverb_gain = 50
    project.tracks[9].reverb_gain = 50
    project.tracks[10].reverb_gain = 100
    project.tracks[11].reverb_gain = 100
    project.tracks[12].reverb_gain = 100
    project.tracks[13].reverb_gain = 100
    project.tracks[14].reverb_gain = 100
    project.tracks[15].reverb_gain = 300

    # eqhigh_on
    project.tracks[0].eqhigh_on = False
    project.tracks[1].eqhigh_on = False
    project.tracks[2].eqhigh_on = False
    project.tracks[3].eqhigh_on = False
    project.tracks[4].eqhigh_on = False
    project.tracks[5].eqhigh_on = False
    project.tracks[6].eqhigh_on = False
    project.tracks[7].eqhigh_on = False
    project.tracks[8].eqhigh_on = False
    project.tracks[9].eqhigh_on = False
    project.tracks[10].eqhigh_on = True
    project.tracks[11].eqhigh_on = True
    project.tracks[12].eqhigh_on = True
    project.tracks[13].eqhigh_on = True
    project.tracks[14].eqhigh_on = True
    project.tracks[15].eqhigh_on = True

    # eqhigh_freq
    with pytest.raises(ValueError):
        project.tracks[0].eqhigh_freq = "wrong value"
    project.tracks[0].eqhigh_freq = 500
    project.tracks[1].eqhigh_freq = 630
    project.tracks[2].eqhigh_freq = 800
    project.tracks[3].eqhigh_freq = 1000
    project.tracks[4].eqhigh_freq = 1300
    project.tracks[5].eqhigh_freq = 1600
    project.tracks[6].eqhigh_freq = 2000
    project.tracks[7].eqhigh_freq = 2500
    project.tracks[8].eqhigh_freq = 3200
    project.tracks[9].eqhigh_freq = 4000
    project.tracks[10].eqhigh_freq = 5000
    project.tracks[11].eqhigh_freq = 6300
    project.tracks[12].eqhigh_freq = 8000
    project.tracks[13].eqhigh_freq = 10000
    project.tracks[14].eqhigh_freq = 12500
    project.tracks[15].eqhigh_freq = 16000

    # eqhigh_gain
    project.tracks[0].eqhigh_gain = -100
    project.tracks[1].eqhigh_gain = -12
    project.tracks[2].eqhigh_gain = -12
    project.tracks[3].eqhigh_gain = -12
    project.tracks[4].eqhigh_gain = -12
    project.tracks[5].eqhigh_gain = 0
    project.tracks[6].eqhigh_gain = 0
    project.tracks[7].eqhigh_gain = 0
    project.tracks[8].eqhigh_gain = 0
    project.tracks[9].eqhigh_gain = 0
    project.tracks[10].eqhigh_gain = 12
    project.tracks[11].eqhigh_gain = 12
    project.tracks[12].eqhigh_gain = 12
    project.tracks[13].eqhigh_gain = 12
    project.tracks[14].eqhigh_gain = 12
    project.tracks[15].eqhigh_gain = 100

    # eqmid_on
    project.tracks[0].eqmid_on = False
    project.tracks[1].eqmid_on = False
    project.tracks[2].eqmid_on = False
    project.tracks[3].eqmid_on = False
    project.tracks[4].eqmid_on = False
    project.tracks[5].eqmid_on = False
    project.tracks[6].eqmid_on = False
    project.tracks[7].eqmid_on = False
    project.tracks[8].eqmid_on = False
    project.tracks[9].eqmid_on = False
    project.tracks[10].eqmid_on = True
    project.tracks[11].eqmid_on = True
    project.tracks[12].eqmid_on = True
    project.tracks[13].eqmid_on = True
    project.tracks[14].eqmid_on = True
    project.tracks[15].eqmid_on = True

    # eqmid_freq
    with pytest.raises(ValueError):
        project.tracks[0].eqmid_freq = "wrong value"
    project.tracks[0].eqmid_freq = 500
    project.tracks[1].eqmid_freq = 630
    project.tracks[2].eqmid_freq = 800
    project.tracks[3].eqmid_freq = 1000
    project.tracks[4].eqmid_freq = 1300
    project.tracks[5].eqmid_freq = 1600
    project.tracks[6].eqmid_freq = 2000
    project.tracks[7].eqmid_freq = 2500
    project.tracks[8].eqmid_freq = 3200
    project.tracks[9].eqmid_freq = 4000
    project.tracks[10].eqmid_freq = 5000
    project.tracks[11].eqmid_freq = 6300
    project.tracks[12].eqmid_freq = 8000
    project.tracks[13].eqmid_freq = 10000
    project.tracks[14].eqmid_freq = 12500
    project.tracks[15].eqmid_freq = 16000

    # eqmid_qfactor
    project.tracks[0].eqmid_qfactor = -3
    project.tracks[1].eqmid_qfactor = 0.1
    project.tracks[2].eqmid_qfactor = 0.1
    project.tracks[3].eqmid_qfactor = 0.1
    project.tracks[4].eqmid_qfactor = 0.1
    project.tracks[5].eqmid_qfactor = 0.5
    project.tracks[6].eqmid_qfactor = 0.5
    project.tracks[7].eqmid_qfactor = 0.5
    project.tracks[8].eqmid_qfactor = 0.5
    project.tracks[9].eqmid_qfactor = 0.5
    project.tracks[10].eqmid_qfactor = 1
    project.tracks[11].eqmid_qfactor = 1
    project.tracks[12].eqmid_qfactor = 1
    project.tracks[13].eqmid_qfactor = 1
    project.tracks[14].eqmid_qfactor = 1
    project.tracks[15].eqmid_qfactor = 3

    # eqmid_gain
    project.tracks[0].eqmid_gain = -100
    project.tracks[1].eqmid_gain = -12
    project.tracks[2].eqmid_gain = -12
    project.tracks[3].eqmid_gain = -12
    project.tracks[4].eqmid_gain = -12
    project.tracks[5].eqmid_gain = 0
    project.tracks[6].eqmid_gain = 0
    project.tracks[7].eqmid_gain = 0
    project.tracks[8].eqmid_gain = 0
    project.tracks[9].eqmid_gain = 0
    project.tracks[10].eqmid_gain = 12
    project.tracks[11].eqmid_gain = 12
    project.tracks[12].eqmid_gain = 12
    project.tracks[13].eqmid_gain = 12
    project.tracks[14].eqmid_gain = 12
    project.tracks[15].eqmid_gain = 100

    # eqlow_on
    project.tracks[0].eqlow_on = False
    project.tracks[1].eqlow_on = False
    project.tracks[2].eqlow_on = False
    project.tracks[3].eqlow_on = False
    project.tracks[4].eqlow_on = False
    project.tracks[5].eqlow_on = False
    project.tracks[6].eqlow_on = False
    project.tracks[7].eqlow_on = False
    project.tracks[8].eqlow_on = False
    project.tracks[9].eqlow_on = False
    project.tracks[10].eqlow_on = True
    project.tracks[11].eqlow_on = True
    project.tracks[12].eqlow_on = True
    project.tracks[13].eqlow_on = True
    project.tracks[14].eqlow_on = True
    project.tracks[15].eqlow_on = True

    # eqlow_freq
    with pytest.raises(ValueError):
        project.tracks[0].eqlow_freq = "wrong value"
    project.tracks[0].eqlow_freq = 40
    project.tracks[1].eqlow_freq = 50
    project.tracks[2].eqlow_freq = 63
    project.tracks[3].eqlow_freq = 100
    project.tracks[4].eqlow_freq = 125
    project.tracks[5].eqlow_freq = 160
    project.tracks[6].eqlow_freq = 200
    project.tracks[7].eqlow_freq = 250
    project.tracks[8].eqlow_freq = 315
    project.tracks[9].eqlow_freq = 400
    project.tracks[10].eqlow_freq = 500
    project.tracks[11].eqlow_freq = 630
    project.tracks[12].eqlow_freq = 800
    project.tracks[13].eqlow_freq = 1000
    project.tracks[14].eqlow_freq = 1300
    project.tracks[15].eqlow_freq = 1600

    # eqlow_gain
    project.tracks[0].eqlow_gain = -100
    project.tracks[1].eqlow_gain = -12
    project.tracks[2].eqlow_gain = -12
    project.tracks[3].eqlow_gain = -12
    project.tracks[4].eqlow_gain = -12
    project.tracks[5].eqlow_gain = 0
    project.tracks[6].eqlow_gain = 0
    project.tracks[7].eqlow_gain = 0
    project.tracks[8].eqlow_gain = 0
    project.tracks[9].eqlow_gain = 0
    project.tracks[10].eqlow_gain = 12
    project.tracks[11].eqlow_gain = 12
    project.tracks[12].eqlow_gain = 12
    project.tracks[13].eqlow_gain = 12
    project.tracks[14].eqlow_gain = 12
    project.tracks[15].eqlow_gain = 100

    # Master track
    project.master.file = "MASTR000.WAV"
    project.master.fader = 88

    new_prjname = __testdir__ / "data" / "new_prjdata.zdt"
    with zoomrlib.open(new_prjname, "w") as file:
        zoomrlib.project.dump(project, file)

    with zoomrlib.open(new_prjname, "r") as file:
        project = zoomrlib.project.load(file)

    # Project
    assert project.name == "NEWPROJ0"
    assert project.header == "ZOOM R-16  PROJECT DATA VER0002"
    assert project.bitlength == 24
    assert project.protected
    assert not project.insert_effect_on

    # Status check
    assert project.tracks[0].status == zoomrlib.REC
    assert project.tracks[1].status == zoomrlib.PLAY
    assert project.tracks[2].status == zoomrlib.MUTE
    assert project.tracks[3].status == zoomrlib.REC
    assert project.tracks[4].status == zoomrlib.PLAY
    assert project.tracks[5].status == zoomrlib.MUTE
    assert project.tracks[6].status == zoomrlib.REC
    assert project.tracks[7].status == zoomrlib.PLAY
    assert project.tracks[8].status == zoomrlib.MUTE
    assert project.tracks[9].status == zoomrlib.REC
    assert project.tracks[10].status == zoomrlib.PLAY
    assert project.tracks[11].status == zoomrlib.MUTE
    assert project.tracks[12].status == zoomrlib.REC
    assert project.tracks[13].status == zoomrlib.PLAY
    assert project.tracks[14].status == zoomrlib.MUTE
    assert project.tracks[15].status == zoomrlib.REC

    # Stereo check
    assert not project.tracks[0].stereo_on
    assert not project.tracks[1].stereo_on
    assert not project.tracks[2].stereo_on
    assert not project.tracks[3].stereo_on
    assert not project.tracks[4].stereo_on
    assert not project.tracks[5].stereo_on
    assert not project.tracks[6].stereo_on
    assert not project.tracks[7].stereo_on
    assert not project.tracks[8].stereo_on
    assert not project.tracks[9].stereo_on
    assert project.tracks[10].stereo_on
    assert project.tracks[11].stereo_on
    assert project.tracks[12].stereo_on
    assert project.tracks[13].stereo_on
    assert project.tracks[14].stereo_on
    assert project.tracks[15].stereo_on

    # Inversion check
    assert not project.tracks[0].invert_on
    assert not project.tracks[1].invert_on
    assert not project.tracks[2].invert_on
    assert not project.tracks[3].invert_on
    assert not project.tracks[4].invert_on
    assert not project.tracks[5].invert_on
    assert not project.tracks[6].invert_on
    assert not project.tracks[7].invert_on
    assert not project.tracks[8].invert_on
    assert not project.tracks[9].invert_on
    assert project.tracks[10].invert_on
    assert project.tracks[11].invert_on
    assert project.tracks[12].invert_on
    assert project.tracks[13].invert_on
    assert project.tracks[14].invert_on
    assert project.tracks[15].invert_on

    # Pan check
    assert project.tracks[0].pan == -50
    assert project.tracks[1].pan == -50
    assert project.tracks[2].pan == -50
    assert project.tracks[3].pan == -50
    assert project.tracks[4].pan == -50
    assert project.tracks[5].pan == 0
    assert project.tracks[6].pan == 0
    assert project.tracks[7].pan == 0
    assert project.tracks[8].pan == 0
    assert project.tracks[9].pan == 0
    assert project.tracks[10].pan == 50
    assert project.tracks[11].pan == 50
    assert project.tracks[12].pan == 50
    assert project.tracks[13].pan == 50
    assert project.tracks[14].pan == 50
    assert project.tracks[15].pan == 50

    # Fader check
    assert project.tracks[0].fader == 0
    assert project.tracks[1].fader == 0
    assert project.tracks[2].fader == 0
    assert project.tracks[3].fader == 0
    assert project.tracks[4].fader == 0
    assert project.tracks[5].fader == 50
    assert project.tracks[6].fader == 50
    assert project.tracks[7].fader == 50
    assert project.tracks[8].fader == 50
    assert project.tracks[9].fader == 50
    assert project.tracks[10].fader == 127
    assert project.tracks[11].fader == 127
    assert project.tracks[12].fader == 127
    assert project.tracks[13].fader == 127
    assert project.tracks[14].fader == 127
    assert project.tracks[15].fader == 127

    # chorus_on
    assert not project.tracks[0].chorus_on
    assert not project.tracks[1].chorus_on
    assert not project.tracks[2].chorus_on
    assert not project.tracks[3].chorus_on
    assert not project.tracks[4].chorus_on
    assert not project.tracks[5].chorus_on
    assert not project.tracks[6].chorus_on
    assert not project.tracks[7].chorus_on
    assert not project.tracks[8].chorus_on
    assert not project.tracks[9].chorus_on
    assert project.tracks[10].chorus_on
    assert project.tracks[11].chorus_on
    assert project.tracks[12].chorus_on
    assert project.tracks[13].chorus_on
    assert project.tracks[14].chorus_on
    assert project.tracks[15].chorus_on

    # chorus_gain
    assert project.tracks[0].chorus_gain == 0
    assert project.tracks[1].chorus_gain == 0
    assert project.tracks[2].chorus_gain == 0
    assert project.tracks[3].chorus_gain == 0
    assert project.tracks[4].chorus_gain == 0
    assert project.tracks[5].chorus_gain == 50
    assert project.tracks[6].chorus_gain == 50
    assert project.tracks[7].chorus_gain == 50
    assert project.tracks[8].chorus_gain == 50
    assert project.tracks[9].chorus_gain == 50
    assert project.tracks[10].chorus_gain == 100
    assert project.tracks[11].chorus_gain == 100
    assert project.tracks[12].chorus_gain == 100
    assert project.tracks[13].chorus_gain == 100
    assert project.tracks[14].chorus_gain == 100
    assert project.tracks[15].chorus_gain == 100

    # reverb_on
    assert not project.tracks[0].reverb_on
    assert not project.tracks[1].reverb_on
    assert not project.tracks[2].reverb_on
    assert not project.tracks[3].reverb_on
    assert not project.tracks[4].reverb_on
    assert not project.tracks[5].reverb_on
    assert not project.tracks[6].reverb_on
    assert not project.tracks[7].reverb_on
    assert not project.tracks[8].reverb_on
    assert not project.tracks[9].reverb_on
    assert project.tracks[10].reverb_on
    assert project.tracks[11].reverb_on
    assert project.tracks[12].reverb_on
    assert project.tracks[13].reverb_on
    assert project.tracks[14].reverb_on
    assert project.tracks[15].reverb_on

    # reverb_gain
    assert project.tracks[0].reverb_gain == 0
    assert project.tracks[1].reverb_gain == 0
    assert project.tracks[2].reverb_gain == 0
    assert project.tracks[3].reverb_gain == 0
    assert project.tracks[4].reverb_gain == 0
    assert project.tracks[5].reverb_gain == 50
    assert project.tracks[6].reverb_gain == 50
    assert project.tracks[7].reverb_gain == 50
    assert project.tracks[8].reverb_gain == 50
    assert project.tracks[9].reverb_gain == 50
    assert project.tracks[10].reverb_gain == 100
    assert project.tracks[11].reverb_gain == 100
    assert project.tracks[12].reverb_gain == 100
    assert project.tracks[13].reverb_gain == 100
    assert project.tracks[14].reverb_gain == 100
    assert project.tracks[15].reverb_gain == 100

    # eqhigh_on
    assert not project.tracks[0].eqhigh_on
    assert not project.tracks[1].eqhigh_on
    assert not project.tracks[2].eqhigh_on
    assert not project.tracks[3].eqhigh_on
    assert not project.tracks[4].eqhigh_on
    assert not project.tracks[5].eqhigh_on
    assert not project.tracks[6].eqhigh_on
    assert not project.tracks[7].eqhigh_on
    assert not project.tracks[8].eqhigh_on
    assert not project.tracks[9].eqhigh_on
    assert project.tracks[10].eqhigh_on
    assert project.tracks[11].eqhigh_on
    assert project.tracks[12].eqhigh_on
    assert project.tracks[13].eqhigh_on
    assert project.tracks[14].eqhigh_on
    assert project.tracks[15].eqhigh_on

    # eqhigh_freq
    assert project.tracks[0].eqhigh_freq == 500
    assert project.tracks[1].eqhigh_freq == 630
    assert project.tracks[2].eqhigh_freq == 800
    assert project.tracks[3].eqhigh_freq == 1000
    assert project.tracks[4].eqhigh_freq == 1300
    assert project.tracks[5].eqhigh_freq == 1600
    assert project.tracks[6].eqhigh_freq == 2000
    assert project.tracks[7].eqhigh_freq == 2500
    assert project.tracks[8].eqhigh_freq == 3200
    assert project.tracks[9].eqhigh_freq == 4000
    assert project.tracks[10].eqhigh_freq == 5000
    assert project.tracks[11].eqhigh_freq == 6300
    assert project.tracks[12].eqhigh_freq == 8000
    assert project.tracks[13].eqhigh_freq == 10000
    assert project.tracks[14].eqhigh_freq == 12500
    assert project.tracks[15].eqhigh_freq == 16000

    # eqhigh_gain
    assert project.tracks[0].eqhigh_gain == -12
    assert project.tracks[1].eqhigh_gain == -12
    assert project.tracks[2].eqhigh_gain == -12
    assert project.tracks[3].eqhigh_gain == -12
    assert project.tracks[4].eqhigh_gain == -12
    assert project.tracks[5].eqhigh_gain == 0
    assert project.tracks[6].eqhigh_gain == 0
    assert project.tracks[7].eqhigh_gain == 0
    assert project.tracks[8].eqhigh_gain == 0
    assert project.tracks[9].eqhigh_gain == 0
    assert project.tracks[10].eqhigh_gain == 12
    assert project.tracks[11].eqhigh_gain == 12
    assert project.tracks[12].eqhigh_gain == 12
    assert project.tracks[13].eqhigh_gain == 12
    assert project.tracks[14].eqhigh_gain == 12
    assert project.tracks[15].eqhigh_gain == 12

    # eqmid_on
    assert not project.tracks[0].eqmid_on
    assert not project.tracks[1].eqmid_on
    assert not project.tracks[2].eqmid_on
    assert not project.tracks[3].eqmid_on
    assert not project.tracks[4].eqmid_on
    assert not project.tracks[5].eqmid_on
    assert not project.tracks[6].eqmid_on
    assert not project.tracks[7].eqmid_on
    assert not project.tracks[8].eqmid_on
    assert not project.tracks[9].eqmid_on
    assert project.tracks[10].eqmid_on
    assert project.tracks[11].eqmid_on
    assert project.tracks[12].eqmid_on
    assert project.tracks[13].eqmid_on
    assert project.tracks[14].eqmid_on
    assert project.tracks[15].eqmid_on

    # eqmid_freq
    assert project.tracks[0].eqmid_freq == 500
    assert project.tracks[1].eqmid_freq == 630
    assert project.tracks[2].eqmid_freq == 800
    assert project.tracks[3].eqmid_freq == 1000
    assert project.tracks[4].eqmid_freq == 1300
    assert project.tracks[5].eqmid_freq == 1600
    assert project.tracks[6].eqmid_freq == 2000
    assert project.tracks[7].eqmid_freq == 2500
    assert project.tracks[8].eqmid_freq == 3200
    assert project.tracks[9].eqmid_freq == 4000
    assert project.tracks[10].eqmid_freq == 5000
    assert project.tracks[11].eqmid_freq == 6300
    assert project.tracks[12].eqmid_freq == 8000
    assert project.tracks[13].eqmid_freq == 10000
    assert project.tracks[14].eqmid_freq == 12500
    assert project.tracks[15].eqmid_freq == 16000

    # eqmid_qfactor
    assert project.tracks[0].eqmid_qfactor == 0.1
    assert project.tracks[1].eqmid_qfactor == 0.1
    assert project.tracks[2].eqmid_qfactor == 0.1
    assert project.tracks[3].eqmid_qfactor == 0.1
    assert project.tracks[4].eqmid_qfactor == 0.1
    assert project.tracks[5].eqmid_qfactor == 0.5
    assert project.tracks[6].eqmid_qfactor == 0.5
    assert project.tracks[7].eqmid_qfactor == 0.5
    assert project.tracks[8].eqmid_qfactor == 0.5
    assert project.tracks[9].eqmid_qfactor == 0.5
    assert project.tracks[10].eqmid_qfactor == 1
    assert project.tracks[11].eqmid_qfactor == 1
    assert project.tracks[12].eqmid_qfactor == 1
    assert project.tracks[13].eqmid_qfactor == 1
    assert project.tracks[14].eqmid_qfactor == 1
    assert project.tracks[15].eqmid_qfactor == 1

    # eqmid_gain
    assert project.tracks[0].eqmid_gain == -12
    assert project.tracks[1].eqmid_gain == -12
    assert project.tracks[2].eqmid_gain == -12
    assert project.tracks[3].eqmid_gain == -12
    assert project.tracks[4].eqmid_gain == -12
    assert project.tracks[5].eqmid_gain == 0
    assert project.tracks[6].eqmid_gain == 0
    assert project.tracks[7].eqmid_gain == 0
    assert project.tracks[8].eqmid_gain == 0
    assert project.tracks[9].eqmid_gain == 0
    assert project.tracks[10].eqmid_gain == 12
    assert project.tracks[11].eqmid_gain == 12
    assert project.tracks[12].eqmid_gain == 12
    assert project.tracks[13].eqmid_gain == 12
    assert project.tracks[14].eqmid_gain == 12
    assert project.tracks[15].eqmid_gain == 12

    # eqlow_on
    assert not project.tracks[0].eqlow_on
    assert not project.tracks[1].eqlow_on
    assert not project.tracks[2].eqlow_on
    assert not project.tracks[3].eqlow_on
    assert not project.tracks[4].eqlow_on
    assert not project.tracks[5].eqlow_on
    assert not project.tracks[6].eqlow_on
    assert not project.tracks[7].eqlow_on
    assert not project.tracks[8].eqlow_on
    assert not project.tracks[9].eqlow_on
    assert project.tracks[10].eqlow_on
    assert project.tracks[11].eqlow_on
    assert project.tracks[12].eqlow_on
    assert project.tracks[13].eqlow_on
    assert project.tracks[14].eqlow_on
    assert project.tracks[15].eqlow_on

    # eqlow_freq
    assert project.tracks[0].eqlow_freq == 40
    assert project.tracks[1].eqlow_freq == 50
    assert project.tracks[2].eqlow_freq == 63
    assert project.tracks[3].eqlow_freq == 100
    assert project.tracks[4].eqlow_freq == 125
    assert project.tracks[5].eqlow_freq == 160
    assert project.tracks[6].eqlow_freq == 200
    assert project.tracks[7].eqlow_freq == 250
    assert project.tracks[8].eqlow_freq == 315
    assert project.tracks[9].eqlow_freq == 400
    assert project.tracks[10].eqlow_freq == 500
    assert project.tracks[11].eqlow_freq == 630
    assert project.tracks[12].eqlow_freq == 800
    assert project.tracks[13].eqlow_freq == 1000
    assert project.tracks[14].eqlow_freq == 1300
    assert project.tracks[15].eqlow_freq == 1600

    # eqlow_gain
    assert project.tracks[0].eqlow_gain == -12
    assert project.tracks[1].eqlow_gain == -12
    assert project.tracks[2].eqlow_gain == -12
    assert project.tracks[3].eqlow_gain == -12
    assert project.tracks[4].eqlow_gain == -12
    assert project.tracks[5].eqlow_gain == 0
    assert project.tracks[6].eqlow_gain == 0
    assert project.tracks[7].eqlow_gain == 0
    assert project.tracks[8].eqlow_gain == 0
    assert project.tracks[9].eqlow_gain == 0
    assert project.tracks[10].eqlow_gain == 12
    assert project.tracks[11].eqlow_gain == 12
    assert project.tracks[12].eqlow_gain == 12
    assert project.tracks[13].eqlow_gain == 12
    assert project.tracks[14].eqlow_gain == 12
    assert project.tracks[15].eqlow_gain == 12

    # Master Track
    assert project.master.file == "MASTR000.WAV"
    assert project.master.fader == 88

    # Track 1
    assert project.tracks[0].file == "BASS-001.WAV"
    new_prjname.unlink()
