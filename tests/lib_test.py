# Copyright 2019-2020 Rémy Taymans <remytms@tsmail.eu>
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

"""Test for zoomrlib library"""

from pathlib import Path

import pytest

import zoomrlib
from zoomrlib.effect import convert_binary_to_int

__testdir__ = Path(__file__).parent


def test_open_raises():
    """Test open function"""
    with pytest.raises(TypeError):
        zoomrlib.open("filename", 123)
    with pytest.raises(ValueError):
        zoomrlib.open("filename", "rw")
        zoomrlib.open("filename", "rb")
        zoomrlib.open("filename", "r+")
        zoomrlib.open("filename", "t")
        zoomrlib.open("filename", "a")


def test_dump_raises():
    """Test dump function"""
    with pytest.raises(TypeError):
        zoomrlib.project.dump("", None)


def test_project_init():
    """Test project creation"""
    with pytest.raises(TypeError):
        zoomrlib.Project("")
    empty_project = zoomrlib.Project()
    assert empty_project.name == zoomrlib.Project.default_name
    prjname = __testdir__ / "data" / "PRJDATA.ZDT"
    with open(prjname, "rb") as file:
        project = zoomrlib.Project(bytearray(file.read()))
    assert project.name == "PRJ028"


def test_default_project():
    """Test creation of a default project"""
    prjname = __testdir__ / "data" / "test_default.zdt"
    project = zoomrlib.Project()

    with zoomrlib.open(prjname, "w") as file:
        zoomrlib.project.dump(project, file)

    with zoomrlib.open(prjname, "r") as file:
        project = zoomrlib.project.load(file)
    assert project.header == project.default_header
    assert project.name == project.default_name
    assert project.bitlength == project.default_bitlength
    assert project.protected == project.default_protected
    assert project.insert_effect_on == project.default_insert_effect_on
    for track in project.tracks:
        assert track.file == track.default_file
        assert track.status == track.default_status
        assert track.stereo_on == track.default_stereo_on
        assert track.invert_on == track.default_invert_on
        assert track.pan == track.default_pan
        assert track.fader == track.default_fader
        assert track.chorus_on == track.default_chorus_on
        assert track.chorus_gain == track.default_chorus_gain
        assert track.reverb_on == track.default_reverb_on
        assert track.reverb_gain == track.default_reverb_gain
        assert track.eqhigh_on == track.default_eqhigh_on
        assert track.eqhigh_freq == track.default_eqhigh_freq
        assert track.eqhigh_gain == track.default_eqhigh_gain
        assert track.eqmid_on == track.default_eqmid_on
        assert track.eqmid_freq == track.default_eqmid_freq
        assert track.eqmid_qfactor == track.default_eqmid_qfactor
        assert track.eqmid_gain == track.default_eqmid_gain
        assert track.eqlow_on == track.default_eqlow_on
        assert track.eqlow_freq == track.default_eqlow_freq
        assert track.eqlow_gain == track.default_eqlow_gain
    assert project.master.file == project.master.default_file
    assert project.master.fader == project.master.default_fader
    prjname.unlink()


def test_default_values_project():
    """
    Test that default values corresponds to the default value of the
    device.
    """
    prjname = __testdir__ / "data" / "DEFAULT.ZDT"

    with zoomrlib.open(prjname, "r") as file:
        project = zoomrlib.project.load(file)
    assert project.header == project.default_header
    # Project name of this project file is "PROJ001", which is not the
    # default value for a project name. A default project name should be
    # "PROJ000". So this value is not tested here.
    assert project.bitlength == project.default_bitlength
    assert project.protected == project.default_protected
    assert project.insert_effect_on == project.default_insert_effect_on
    for track in project.tracks:
        assert track.file == track.default_file
        assert track.status == track.default_status
        assert track.stereo_on == track.default_stereo_on
        assert track.invert_on == track.default_invert_on
        assert track.pan == track.default_pan
        assert track.fader == track.default_fader
        assert track.chorus_on == track.default_chorus_on
        assert track.chorus_gain == track.default_chorus_gain
        assert track.reverb_on == track.default_reverb_on
        assert track.reverb_gain == track.default_reverb_gain
        assert track.eqhigh_on == track.default_eqhigh_on
        assert track.eqhigh_freq == track.default_eqhigh_freq
        assert track.eqhigh_gain == track.default_eqhigh_gain
        assert track.eqmid_on == track.default_eqmid_on
        assert track.eqmid_freq == track.default_eqmid_freq
        assert track.eqmid_qfactor == track.default_eqmid_qfactor
        assert track.eqmid_gain == track.default_eqmid_gain
        assert track.eqlow_on == track.default_eqlow_on
        assert track.eqlow_freq == track.default_eqlow_freq
        assert track.eqlow_gain == track.default_eqlow_gain
    assert project.master.file == project.master.default_file
    assert project.master.fader == project.master.default_fader


def test_project():
    """Test Project with PRJDATA.ZDT file"""
    prjname = __testdir__ / "data" / "PRJDATA.ZDT"
    with open(prjname, "rb") as file:
        project = zoomrlib.project.load(file)
    # Project
    assert project.name == "PRJ028"
    assert project.header == "ZOOM R-16  PROJECT DATA VER0001"
    assert not project.protected
    assert project.bitlength == 16
    # Track 1
    assert project.tracks[0].file == "MONO-000.WAV"
    assert not project.tracks[0].stereo_on
    assert not project.tracks[0].invert_on
    assert project.tracks[0].pan == 50
    assert project.tracks[0].fader == 47
    assert project.tracks[0].chorus_on
    assert project.tracks[0].chorus_gain == 0
    assert project.tracks[0].reverb_on
    assert project.tracks[0].reverb_gain == 10
    assert project.tracks[0].eqhigh_on
    assert project.tracks[0].eqmid_on
    assert project.tracks[0].eqlow_on
    # Track 2
    assert project.tracks[1].file == "MONO-001.WAV"
    assert not project.tracks[1].stereo_on
    assert not project.tracks[1].invert_on
    assert project.tracks[1].pan == -50
    assert project.tracks[1].fader == 45
    assert project.tracks[1].chorus_on
    assert project.tracks[1].chorus_gain == 0
    assert project.tracks[1].reverb_on
    assert project.tracks[1].reverb_gain == 10
    assert project.tracks[1].eqhigh_on
    assert project.tracks[1].eqmid_on
    assert project.tracks[1].eqlow_on
    # Track 3
    assert project.tracks[2].file == "MONO-002.WAV"
    assert not project.tracks[2].stereo_on
    assert not project.tracks[2].invert_on
    assert project.tracks[2].pan == 0
    assert project.tracks[2].fader == 87
    assert project.tracks[2].chorus_on
    assert project.tracks[2].chorus_gain == 0
    assert project.tracks[2].reverb_on
    assert project.tracks[2].reverb_gain == 0
    assert project.tracks[2].eqhigh_on
    assert project.tracks[2].eqmid_on
    assert project.tracks[2].eqlow_on
    # Track 4
    assert project.tracks[3].file == "MONO-003.WAV"
    assert not project.tracks[3].stereo_on
    assert not project.tracks[3].invert_on
    assert project.tracks[3].pan == -15
    assert project.tracks[3].fader == 75
    assert project.tracks[3].chorus_on
    assert project.tracks[3].chorus_gain == 0
    assert project.tracks[3].reverb_on
    assert project.tracks[3].reverb_gain == 20
    assert project.tracks[3].eqhigh_on
    assert project.tracks[3].eqmid_on
    assert project.tracks[3].eqlow_on
    # Master
    assert project.master.file == "MASTR000.WAV"
    with pytest.raises(AttributeError):
        project.master.pan  # pylint: disable=no-member,pointless-statement
    assert project.master.fader == 98


def test_project2():
    """Test Project with PRJDATA2.ZDT file"""
    prjname = __testdir__ / "data" / "PRJDATA2.ZDT"
    with open(prjname, "rb") as file:
        project = zoomrlib.project.load(file)
    # Project
    assert project.name == "PRJ006"
    assert project.bitlength == 16
    # EQ High Frequency
    assert project.tracks[0].eqhigh_freq == 1000
    assert project.tracks[1].eqhigh_freq == 1300
    assert project.tracks[2].eqhigh_freq == 1600
    assert project.tracks[3].eqhigh_freq == 2000
    assert project.tracks[4].eqhigh_freq == 2500
    assert project.tracks[5].eqhigh_freq == 3200
    assert project.tracks[6].eqhigh_freq == 4000
    assert project.tracks[7].eqhigh_freq == 5000
    # EQ High Gain
    assert project.tracks[0].eqhigh_gain == 0
    assert project.tracks[1].eqhigh_gain == 0
    assert project.tracks[2].eqhigh_gain == 0
    assert project.tracks[3].eqhigh_gain == 0
    assert project.tracks[4].eqhigh_gain == 0
    assert project.tracks[5].eqhigh_gain == 0
    assert project.tracks[6].eqhigh_gain == 0
    assert project.tracks[7].eqhigh_gain == 0
    # EQ Mid Frequency
    assert project.tracks[0].eqmid_freq == 1000
    assert project.tracks[1].eqmid_freq == 1300
    assert project.tracks[2].eqmid_freq == 1600
    assert project.tracks[3].eqmid_freq == 2000
    assert project.tracks[4].eqmid_freq == 2500
    assert project.tracks[5].eqmid_freq == 3200
    assert project.tracks[6].eqmid_freq == 4000
    assert project.tracks[7].eqmid_freq == 5000
    # EQ Mid Q-Factor
    assert project.tracks[0].eqmid_qfactor == 0.1
    assert project.tracks[1].eqmid_qfactor == 0.2
    assert project.tracks[2].eqmid_qfactor == 0.3
    assert project.tracks[3].eqmid_qfactor == 0.4
    assert project.tracks[4].eqmid_qfactor == 0.5
    assert project.tracks[5].eqmid_qfactor == 0.6
    assert project.tracks[6].eqmid_qfactor == 0.7
    assert project.tracks[7].eqmid_qfactor == 0.8
    # EQ Mid Gain
    assert project.tracks[0].eqmid_gain == -12
    assert project.tracks[1].eqmid_gain == 0
    assert project.tracks[2].eqmid_gain == 12
    assert project.tracks[3].eqlow_gain == 0
    assert project.tracks[4].eqlow_gain == 0
    assert project.tracks[5].eqlow_gain == 0
    assert project.tracks[6].eqlow_gain == 0
    assert project.tracks[7].eqlow_gain == 0
    # EQ Low Frequency
    assert project.tracks[0].eqlow_freq == 40
    assert project.tracks[1].eqlow_freq == 50
    assert project.tracks[2].eqlow_freq == 63
    assert project.tracks[3].eqlow_freq == 80
    assert project.tracks[4].eqlow_freq == 100
    assert project.tracks[5].eqlow_freq == 125
    assert project.tracks[6].eqlow_freq == 160
    assert project.tracks[7].eqlow_freq == 200
    # EQ Low Gain
    assert project.tracks[0].eqlow_gain == 0
    assert project.tracks[1].eqlow_gain == 0
    assert project.tracks[2].eqlow_gain == 0
    assert project.tracks[3].eqlow_gain == 0
    assert project.tracks[4].eqlow_gain == 0
    assert project.tracks[5].eqlow_gain == 0
    assert project.tracks[6].eqlow_gain == 0
    assert project.tracks[7].eqlow_gain == 0


def test_project_stereo():
    """Test Project with STEREO.ZDT file"""
    prjname = __testdir__ / "data" / "STEREO.ZDT"
    with open(prjname, "rb") as file:
        project = zoomrlib.project.load(file)
    assert project.protected
    assert project.bitlength == 24
    assert project.insert_effect_on
    assert project.tracks[0].stereo_on
    assert project.tracks[0].eqhigh_gain == 5
    assert project.tracks[0].eqmid_gain == 5
    assert project.tracks[0].eqlow_gain == 5
    # The following tracks are linked stereo
    # The first track properties affects the two tracks, but the second
    # tracks keeps its settings in the project file.
    assert project.tracks[0].fader == 102
    assert project.tracks[1].fader == 80


def test_project_green_status():
    """Test Project with GREEN.ZDT file"""
    prjname = __testdir__ / "data" / "GREEN.ZDT"
    with open(prjname, "rb") as file:
        project = zoomrlib.project.load(file)
    for track in project.tracks:
        assert track.status == "play"


def test_project_mute_status():
    """Test Project with DOWN.ZDT file"""
    prjname = __testdir__ / "data" / "DOWN.ZDT"
    with open(prjname, "rb") as file:
        project = zoomrlib.project.load(file)
    for track in project.tracks:
        assert track.status == "mute"


def test_project_red_status():
    """Test Project with RED.ZDT file"""
    prjname = __testdir__ / "data" / "RED.ZDT"
    with open(prjname, "rb") as file:
        project = zoomrlib.project.load(file)
    assert project.tracks[0].status == "record"
    assert project.tracks[1].status == "record"
    assert project.tracks[2].status == "record"
    assert project.tracks[3].status == "record"
    assert project.tracks[4].status == "record"
    assert project.tracks[5].status == "record"
    assert project.tracks[6].status == "record"
    assert project.tracks[7].status == "record"
    assert project.tracks[8].status == "play"
    assert project.tracks[9].status == "play"
    assert project.tracks[10].status == "play"
    assert project.tracks[11].status == "play"
    assert project.tracks[12].status == "play"
    assert project.tracks[13].status == "play"
    assert project.tracks[14].status == "play"
    assert project.tracks[15].status == "play"


def test_project_mixed_status():
    """Test Project with MIX.ZDT file"""
    prjname = __testdir__ / "data" / "MIX.ZDT"
    with open(prjname, "rb") as file:
        project = zoomrlib.project.load(file)
    assert project.tracks[0].status == "record"
    assert project.tracks[1].status == "play"
    assert project.tracks[2].status == "mute"
    assert project.tracks[3].status == "record"
    assert project.tracks[4].status == "play"
    assert project.tracks[5].status == "mute"
    assert project.tracks[6].status == "record"
    assert project.tracks[7].status == "play"
    assert project.tracks[8].status == "mute"
    assert project.tracks[9].status == "record"
    assert project.tracks[10].status == "play"
    assert project.tracks[11].status == "mute"
    assert project.tracks[12].status == "record"
    assert project.tracks[13].status == "play"
    assert project.tracks[14].status == "mute"
    assert project.tracks[15].status == "record"


# Tests for Effects


def test_efx_load_efxdata1():
    """Test that we can load EFXDATA1 file and verify"""
    efxpath = __testdir__ / "data" / "EFXDATA1.ZDT"
    with zoomrlib.open(efxpath, "r") as file:
        effect = zoomrlib.effect.load(file)
    assert effect.header == "ZOOM R-16  EFFECT DATA VER0001"
    assert effect.valid_header
    assert effect.send_reverb_on
    assert effect.send_reverb_patch_num == 4
    assert effect.send_reverb_patch_name == "SmallHal"
    assert not effect.send_chorus_on
    assert effect.send_chorus_patch_num is None
    assert effect.send_chorus_patch_name is None


def test_efx_load_efxdata2():
    """Test that we can load EFXDATA2 file and verify"""
    efxpath = __testdir__ / "data" / "EFXDATA2.ZDT"
    with zoomrlib.open(efxpath, "r") as file:
        effect = zoomrlib.effect.load(file)
    assert effect.header == "ZOOM R-16  EFFECT DATA VER0001"
    assert effect.valid_header
    assert not effect.send_reverb_on
    assert effect.send_reverb_patch_num is None
    assert effect.send_reverb_patch_name is None
    assert effect.send_chorus_on
    assert effect.send_chorus_patch_num == 13
    assert effect.send_chorus_patch_name == "Detune"


def test_efx_chg_send_reverb_onoff():
    """Test that we can turn off the SEND REVERB effect"""
    # Load the EFX DATA file #1
    efxpath = __testdir__ / "data" / "EFXDATA1.ZDT"
    with zoomrlib.open(efxpath, "r") as file:
        effect = zoomrlib.effect.load(file)

    # Turn off the SEND REVERB effect
    effect.send_reverb_on = False

    # Verify that the properties are None
    assert effect.send_reverb_patch_num is None
    assert effect.send_reverb_patch_name is None

    # Dump the file to a new output file...
    efxpath = __testdir__ / "data" / "EFXDATA1_MOD.ZDT"
    with open(efxpath, "wb") as file:
        zoomrlib.effect.dump(effect, file)

    # Load it back in (one more time...)
    with zoomrlib.open(efxpath, "r") as file:
        effect = zoomrlib.effect.load(file)

    # Verify that the properties are (still) None
    assert effect.send_reverb_patch_num is None
    assert effect.send_reverb_patch_name is None


def test_efx_chg_send_chorus_onoff():
    """Test that we turn on the SEND CHORUS effect"""
    efxpath = __testdir__ / "data" / "EFXDATA1.ZDT"
    with zoomrlib.open(efxpath, "r") as file:
        effect = zoomrlib.effect.load(file)

    # Turn off the SEND CHORUS effect
    effect.send_chorus_on = True
    assert effect.send_reverb_patch_num == 4
    assert effect.send_reverb_patch_name == "SmallHal"


def test_efx_ssrp_miss_args():
    """Test that we can correctly raise an error when missing SEND REVERB args"""
    efxpath = __testdir__ / "data" / "EFXDATA1.ZDT"
    with zoomrlib.open(efxpath, "r") as file:
        effect = zoomrlib.effect.load(file)

    with pytest.raises(ValueError):
        effect.set_send_reverb_patch()


def test_efx_ssrp_inv_num():
    """Test that we can correctly raise an error when integer is wrong"""
    efxpath = __testdir__ / "data" / "EFXDATA1.ZDT"
    with zoomrlib.open(efxpath, "r") as file:
        effect = zoomrlib.effect.load(file)

    with pytest.raises(ValueError):
        effect.set_send_reverb_patch(patch_num=-1, patch_name="SoftHall")

    with pytest.raises(ValueError):
        effect.set_send_reverb_patch(patch_num=30, patch_name="SoftHall")


def test_efx_ssrp_inv_type():
    """Test that we can correctly raise an error when arg type is wrong"""
    efxpath = __testdir__ / "data" / "EFXDATA1.ZDT"
    with zoomrlib.open(efxpath, "r") as file:
        effect = zoomrlib.effect.load(file)

    with pytest.raises(TypeError):
        effect.set_send_reverb_patch(patch_num="1", patch_name="BrgtRoom")

    with pytest.raises(TypeError):
        effect.set_send_reverb_patch(patch_num=0, patch_name=0)


def test_efx_ssrp_inv_combo():
    """Test that we can correctly raise an error when the combo is wrong"""
    efxpath = __testdir__ / "data" / "EFXDATA1.ZDT"
    with zoomrlib.open(efxpath, "r") as file:
        effect = zoomrlib.effect.load(file)

    with pytest.raises(ValueError):
        effect.set_send_reverb_patch(patch_num=0, patch_name="BrgtRoom")


def test_efx_ssrp_val_combo_both():
    """Test that we can ensure correct behavior when both are set"""
    # Create a new Effect
    effect = zoomrlib.Effect()

    # Set a valid pair
    effect.set_send_reverb_patch(patch_num=1, patch_name="BrgtRoom")

    # Verify that both name and number are correct
    assert effect.send_reverb_patch_num == 1
    assert effect.send_reverb_patch_name == "BrgtRoom"

    # Dump the file to a new output file...
    efxpath = __testdir__ / "data" / "EFXDATA1_MOD.ZDT"
    with open(efxpath, "wb") as file:
        zoomrlib.effect.dump(effect, file)

    # Load it back in (one more time...)
    with zoomrlib.open(efxpath, "r") as file:
        effect = zoomrlib.effect.load(file)

    # Verify that both name and number are correct
    assert effect.send_reverb_patch_num == 1
    assert effect.send_reverb_patch_name == "BrgtRoom"


def test_efx_sscp_miss_args():
    """Test that we can correctly raise an error when missing SEND CHORUS args"""
    effect = zoomrlib.Effect()

    with pytest.raises(ValueError):
        effect.set_send_chorus_patch()


def test_efx_sscp_inv_num():
    """Test that we can correctly raise an error when integer is wrong"""
    effect = zoomrlib.Effect()

    with pytest.raises(ValueError):
        effect.set_send_chorus_patch(patch_num=-1, patch_name="Something")

    with pytest.raises(ValueError):
        effect.set_send_chorus_patch(patch_num=21, patch_name="Dokan")


def test_efx_sscp_inv_type():
    """Test that we can correctly raise an error when type is wrong"""
    efxpath = __testdir__ / "data" / "EFXDATA1.ZDT"
    with zoomrlib.open(efxpath, "r") as file:
        effect = zoomrlib.effect.load(file)

    with pytest.raises(TypeError):
        effect.set_send_chorus_patch(patch_num="1", patch_name="BrgtRoom")

    with pytest.raises(TypeError):
        effect.set_send_chorus_patch(patch_num=0, patch_name=0)


def test_efx_sscp_inv_combo():
    """Test that we can correctly raise an error when the combo is wrong"""
    efxpath = __testdir__ / "data" / "EFXDATA1.ZDT"
    with zoomrlib.open(efxpath, "r") as file:
        effect = zoomrlib.effect.load(file)

    with pytest.raises(ValueError):
        effect.set_send_chorus_patch(patch_num=0, patch_name="BrgtRoom")


def test_efx_sscp_val_combo_both():
    """Test that we can ensure correct behavior when both are set"""

    # Create a new Effect
    effect = zoomrlib.Effect()

    # Set a valid pair
    effect.set_send_chorus_patch(patch_num=7, patch_name="DeepCho")

    # Verify that both name and number are correct
    assert effect.send_chorus_patch_num == 7
    assert effect.send_chorus_patch_name == "DeepCho"

    # Dump the file to a new output file...
    efxpath = __testdir__ / "data" / "EFXDATA1_MOD.ZDT"
    with open(efxpath, "wb") as file:
        zoomrlib.effect.dump(effect, file)

    # Load it back in (one more time...)
    with zoomrlib.open(efxpath, "r") as file:
        effect = zoomrlib.effect.load(file)

    # Verify that both name and number are correct
    assert effect.send_chorus_patch_num == 7
    assert effect.send_chorus_patch_name == "DeepCho"


def test_efx_dump_wrong_class():
    """Test the Effect dump function"""
    with pytest.raises(TypeError):
        zoomrlib.effect.dump("", None)


def test_internal_functions():
    """Test the behavior of internal functions"""
    test_data = bytes(b"DEADCAFE")

    with pytest.raises(ValueError):
        convert_binary_to_int(test_data)


def test_efx_with_no_bytes():
    """Test that Effect works correctly with no args"""
    # Create a new Effect instance
    effect = zoomrlib.Effect()

    # Check our header...
    assert effect.valid_header
